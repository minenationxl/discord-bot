# Discord Bot
Discord bot for the MineNationXL Discord server.

## Setup
- Install **TypeScript**: `npm i typescript -g` or `yarn global add typescript`
- Install all dependencies: `npm install`
- Configure all required fields in `config.json` (example in `config.example.json`)
- Start the bot: `npm start`

## Contribution
* New features and bug fixes should be developed and tested in a new branch.
* Feature branches should always be merged with development.
* Commits should be prefixed using [GitMoji](https://gitmoji.carloscuesta.me/).